<?php
/**
 * FAE 
 */
namespace FAE\auth_local;

use FAE\schema\model\model;

class auth_session extends model {
  
  var $_model     = 'auth_session';
  var $_modelFile = __DIR__ . '/../models/auth_session.json';
  var $_rest = false;
  
}