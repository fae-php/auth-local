<?php
namespace FAE\auth_local;

use FAE\auth\authInterface;
use FAE\auth\authProvider;
use FAE\user\user;
use FAE\rest\rest;
use FAE\permissions\user_perm_group;
use FAE\permissions\perm_assign;
use FAE\permissions\perm_group;
use FAE\mailer\mailer;

class auth extends authProvider implements authInterface {
  
  var $_rest = false;
  
  const TIMEOUT = 60 * 60 * 24;
  const HASH_LEN = 32;
  
  static $default_pgid;
  
  public function getSession()
  {
    global $config;
    if(!isset($_COOKIE['ussid']) || !strlen($_COOKIE['ussid']) == self::HASH_LEN){
      return false;
    }
    $session = new auth_session();
    $existingSession = $session->get( ['session_id' => session_id(), 'session_hash' => $this->encrypt($_COOKIE['ussid']), 'ip' => $_SERVER['REMOTE_ADDR'] ] );
    if(!$existingSession->rowCount()){
      return false;
    }
    $this->user_id = $existingSession->fetch()['user_id'];
    if(! setcookie( 'ussid', $_COOKIE['ussid'], time()+(defined('ARIA_SESSION_LENGTH') ? ARIA_SESSION_LENGTH : self::TIMEOUT), "/".$config->path ) ){
      echo 'could not update cookie';
    }
    return true;
  }
  
  public function login( string $id, string $password, string $column = 'email' )
  {
    global $config;
    $uh = new user();
    $userRow = $uh->get( [ $column => $id ] )->fetch();
    if(!$userRow){
      $this->error = 101;
      return false;
    }
    $ph = new user_password();
    $passwordRow = $ph->get( [ 'user_id' => $userRow['id'] ] )->fetch();
    if(!$passwordRow){
      $this->error = 105;
      return false;
    }
    if(!$ph->verifyPassword($password, $passwordRow['password'])){
      $this->error = 102;
      return false;
    }
    $ussid = $this->randGen(self::HASH_LEN);
    try {
      if( !setcookie( 'ussid', $ussid, time()+(defined('ARIA_SESSION_LENGTH') ? ARIA_SESSION_LENGTH : self::TIMEOUT), $config->path ) ){
        $this->error = 106;
        return false;
      }
    } catch (\Exception $e) {
      $this->error = 103;
      return false;
    }
    $sh = new auth_session();
    try {
      $sh->insert([
        'user_id'       => $userRow['id'],
        'session_id'    => session_id(),
        'ip'            => $_SERVER['REMOTE_ADDR'],
        'session_hash'  => self::encrypt( $ussid ),
      ]);
    } catch (\Exception $e) {
      print_r($e);
      $this->error = 104;
      return false;
    }
    $this->user_id = $userRow['id'];
    $upgh = new user_perm_group();
    $permGroup = $upgh->get( [ 'user_id' => $userRow['id'] ] )->fetch();
    if($permGroup){
      $this->perm_group_id = $permGroup['perm_group_id'];
    }
    return true;
	}
	
	static function apiLogin()
	{
  	  $auth = new self();
  	  $response = new \StdClass();
  	  if( !$auth->login( $_POST['id'], $_POST['password'] ) ){
    	  $response->error = (object) [ 'message' => 'An error occurred during login.', 'code' => $auth->error ];
  	  } else {
    	  $response->success = true;
  	  }
  	  rest::output( $response );
	}
	
	static function logout()
	{
    global $config;
    unset($_COOKIE['ussid']);
    setcookie('ussid', '', time() - 3600, $config->path);
		session_destroy();
		header("Location: ".$config->path);
		die();
		return true;
	}
	
	private function encrypt( string $string )
	{
  	  return hash('sha256', SALT.$string);
	}
  
  public function redirectLogin()
  {
    global $config;
    $currentPage = $config->root . $config->path . $_SERVER['REQUEST_URI'];
    if( $this->removeQuery($currentPage) != $this->removeQuery($this->loginURL()) ){
      header("Location: ".$this->loginURL($currentPage));
      die();
    }
  }
	
	public function loginURL( string $destination = null )
	{
  	global $config;
    if(is_null($destination)){
      $destination = $config->root . $config->path . $_SERVER['REQUEST_URI']; 
    } else {
      $destination = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . '://'.preg_replace('/^https?:\/\/(.*)\/?$/U', '$1', $destination);
    }
    return $config->root . $config->path.'/login?target='.urlencode($destination);
  }
  
  protected function removeQuery( string $url )
  {
    return explode('?', $url)[0];
  }
	
	static function apiReset()
	{
  	  $auth = new self();
  	  $response = new \StdClass();
  	  $uh = new user();
  	  $user = $uh->get( [ 'email' => $_POST['email'] ] )->fetch();
  	  if( !$auth->resetPassword( (int) $user['id'] ) ){
    	  $response->error = (object) [ 'message' => 'An error occurred resetting your password', 'code' => $auth->error ];
    	  return rest::output( $response );
  	  }
    $response->success = true;
  	  return rest::output( $response );
	}
	
	public function resetPassword( int $user_id )
	{
  	  global $config;
  	  // Fetch the user
  	  $uh = new user();
  	  $user = $uh->getById($user_id);
  	  if(!$user){
    	  $this->error = 201;
    	  return false;
  	  }
  	  
  	  // Create and store the code
  	  $code = $this->randGen(4).'-'.$this->randGen(4);
  	  $arh = new auth_reset();
  	  if(!$arh->insert( [ 'user_id' => $user_id, 'code' => $code ] )){
    	  $this->error = 202;
    	  return false;
  	  }
  	  
  	  // Email code to user
  	  $opts = [
      'code'        => $code,
      'first_name'  => $user['first_name'],
      'last_name'   => $user['last_name'],
      'email'       => $user['email'],
      'ip'          => $_SERVER['REMOTE_ADDR'],
      'url'         => "http".($_SERVER['HTTPS'] ? 's' : '')."://{$_SERVER['HTTP_HOST']}/{$config->path}/password?code={$code}",
    ];
    if(!mailer::send($opts['email'], 'Password Reset Verification Code', array('template' => 'password.twig', 'variables' => $opts, 'uid' => $user_id, 'instant' => true))){
      $this->error = 203;
      return false;
    }
    return true;
	}
	
	static function apiSave()
	{
    $auth = new self();
    $response = new \StdClass();
    $ah = new auth_reset();
    $reset = $ah->get( [ 'code' => $_POST['code'] ] )->fetch();
    sleep(5);
    if(!$reset){
      $response->error = (object) [ 'message' => 'An error occurred resetting your password', 'code' => 301 ];
      return rest::output( $response );
    }
    if(strtotime($reset['created']) < time()-(15*60)){
      $ah->delete( [ 'id' => $reset['id'] ] );
      $response->error = (object) [ 'message' => 'An error occurred resetting your password', 'code' => 304 ];
      return rest::output( $response );
    }
    if( !$auth->savePassword( (int) $reset['user_id'], (string) $_POST['password'] ) ){
      $response->error = (object) [ 'message' => 'An error occurred resetting your password', 'code' => $auth->error ];
      return rest::output( $response );
    }
    $ah->delete( [ 'id' => $reset['user_id'] ] );
    $response->success = true;
    return rest::output( $response );
	}
	
	public function savePassword( int $user_id, string $password )
	{
	  $uph = new user_password();
	  $exisiting = $uph->get( ['user_id' => $user_id] );
	  if($exisiting->rowCount()){
  	  if(!$uph->update( ['password' => $password], ['user_id' => $user_id] )){
    	  $this->error = 302;
    	  return false;
  	  }
	  } else {
  	  if(!$uph->insert( ['password' => $password, 'user_id' => $user_id] )){
    	  $this->error = 303;
    	  return false;
  	  }
	  }
	  return true;
	}
  
  static function randGen( int $length = 10, string $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' )
  {
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
  
}