<?php
/**
 * Schema cli manager for FAE
 * Copyright 2019 Callum Smith
 */
namespace FAE\auth_local;

use FAE\user\user;
use FAE\permissions\perm_group;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ChoiceQuestion;

class createUser extends Command {
  
  protected static $defaultName = 'auth:create-user';
  
  protected function configure()
  {
    $this
      ->setDescription('Create a new user')
      ->setHelp('Help?')
      ->addArgument('username', InputArgument::REQUIRED, 'Username')
      ->addArgument('first-name', InputArgument::REQUIRED, 'First Name')
      ->addArgument('last-name', InputArgument::REQUIRED, 'Last Name')
      ->addArgument('email', InputArgument::REQUIRED, 'Email Address')
    ;
  }
  
  protected function execute(InputInterface $input, OutputInterface $output){
    $helper = $this->getHelper('question');
    
    $question = new Question('Please enter the password for this new account: ');
    $question->setHidden(true);
    $question->setHiddenFallback(false);
    $password = $helper->ask($input, $output, $question);
    
    $pgh = new perm_group();
    $permGroups = $pgh->get();
    $groupsArray = [];
    while($row = $permGroups->fetch()){
      $groupsArray[$row['id']] = $row['name'];
    }
    
    $question = new ChoiceQuestion(
      'Please select which permission group to assign this user to: ',
      $groupsArray
    );
    $permGroupId = array_search($helper->ask($input, $output, $question), $groupsArray);
    
    $user = new user();
    $userData = [
      'username' => $input->getArgument('username'),
      'first_name' => $input->getArgument('first-name'),
      'last_name' => $input->getArgument('last-name'),
      'email' => $input->getArgument('email'),
      'perm_group_id' => $permGroupId,
    ];
    $response = $user->insert($userData, [], true, false);
    $ah = new auth();
    $auth = $ah->get();
    $auth->savePassword($response['id'], $password);
    $output->writeLn('Successfully created user account');
  }
  
}