<?php
/**
 * FAE 
 */
namespace FAE\auth_local;

use FAE\schema\model\model;

class user_password extends model {
  
  var $_model     = 'user_password';
  var $_modelFile = __DIR__ . '/../models/user_password.json';
  var $_rest = false;
  
  function setDataHook($data)
  {
    if( array_key_exists('password', $data ) ){
      $data['password'] = $this->encryptPassword( $data['password'] );
    }
    return $data;
  }
	
	protected function encryptPassword( string $password )
	{
  	  return password_hash( SALT.$password, PASSWORD_DEFAULT, [ 'cost' => 11 ] );
	}
	
  function verifyPassword( string $password, string $hash )
	{
  	  return password_verify( SALT.$password, $hash );
	}
  
}