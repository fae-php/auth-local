<?php

namespace FAE\auth_local;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use FAE\fae\fae;
use FAE\fae\routes;

global $config;

$authRoutes = new RouteCollection();
$authRoutes->add('login-view', new Route(
  '/login',
  [
    '_controller'   => '\\FAE\\template\\layout::pageLoader',
    '_page'         => 'login.twig',
    '_layoutPath'   => fae::_path(__DIR__, 'views') . DIRECTORY_SEPARATOR,
  ],
  [],
  [],
  '',
  [],
  ['GET']
));
$authRoutes->add('login-api', new Route(
  "/api/{$config->apiVersion}/login",
  [
    '_controller'   => '\\FAE\\auth_local\\auth::apiLogin',
  ],
  [],
  [],
  '',
  [],
  ['POST']
));
$authRoutes->add('logout', new Route(
  '/logout',
  [
    '_controller'   => '\\FAE\\auth_local\\auth::logout',
  ],
  [],
  [],
  '',
  [],
  ['GET']
));
$authRoutes->add('password-view', new Route(
  '/password',
  [
    '_controller'   => '\\FAE\\template\\layout::pageLoader',
    '_page'         => 'password.twig',
    '_layoutPath'   => fae::_path(__DIR__, 'views') . DIRECTORY_SEPARATOR,
  ],
  [],
  [],
  '',
  [],
  ['GET']
));
$authRoutes->add('password-reset-api', new Route(
  "/api/{$config->apiVersion}/password-reset",
  [
    '_controller'   => '\\FAE\\auth_local\\auth::apiReset',
  ],
  [],
  [],
  '',
  [],
  ['POST']
));
$authRoutes->add('password-save-api', new Route(
  "/api/{$config->apiVersion}/password-save",
  [
    '_controller'   => '\\FAE\\auth_local\\auth::apiSave',
  ],
  [],
  [],
  '',
  [],
  ['POST']
));

routes::addCollection($authRoutes);
