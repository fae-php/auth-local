<?php
/**
 * FAE 
 */
namespace FAE\auth_local;

use FAE\schema\model\model;

class auth_reset extends model {
  
  var $_model     = 'auth_reset';
  var $_modelFile = __DIR__ . '/../models/auth_reset.json';
  var $_rest = false;
  
}